import requests
from decouple import config

from nodes.whstack_sync_dhcp_kea_from_backend import (
    work as whstack_sync_dhcp_kea_from_backend_work,
)

BACKEND_URL = config("BACKEND_URL", default="http://localhost", cast=str)
BACKEND_USER = config("BACKEND_USER", default="username", cast=str)
BACKEND_APIKEY = config("BACKEND_APIKEY", default="SECRET", cast=str)
DHCP_ONLY_NETWORK_IDS = config("DHCP_ONLY_NETWORK_IDS", default=None, cast=str)

INSTANCE_ID = config("INSTANCE_ID", default=0, cast=int)

request_params = {}

if DHCP_ONLY_NETWORK_IDS is not None and DHCP_ONLY_NETWORK_IDS != "None":
    request_params["only_network_ids"] = DHCP_ONLY_NETWORK_IDS

instance_config = requests.get(
    BACKEND_URL + "/api/v1/dhcpkea/" + str(INSTANCE_ID),
    headers={"Authorization": "ApiKey " + BACKEND_USER + ":" + BACKEND_APIKEY},
    params=request_params,
)

if instance_config.status_code != 200:
    print("Deployment failed:\r\n")
    print(instance_config.text)
    exit(instance_config.status_code)

dcmgr_instance_data = instance_config.json()

whstack_sync_dhcp_kea_from_backend_work(dcmgr_instance_data)
