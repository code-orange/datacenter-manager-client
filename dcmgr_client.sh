#!/bin/bash

# VARIABLES
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

cd $SCRIPTPATH

CONF_OK_DHCP4=0
CONF_OK_DHCP6=0

CONF_DIFF_DHCP4=0
CONF_DIFF_DHCP6=0
CONF_DIFF_HOSTS_BLOCKLIST=0

. venv/bin/activate

./venv/bin/python3 /opt/datacenter-manager-client/dcmgr_client.py >/dev/null 2>&1

if [ $? -eq 0 ]; then
    echo OK
else
    echo FAIL
    exit 1
fi

# ===================== DHCP =====================
/usr/sbin/kea-dhcp4 -t /etc/kea/kea-dhcp4.conf.new

if [ $? -eq 0 ]; then
    echo OK
    CONF_OK_DHCP4=1
else
    echo FAIL
fi

/usr/sbin/kea-dhcp6 -t /etc/kea/kea-dhcp6.conf.new

if [ $? -eq 0 ]; then
    echo OK
    CONF_OK_DHCP6=1
else
    echo FAIL
fi

cmp --silent /etc/kea/kea-dhcp4.conf.new /etc/kea/kea-dhcp4.conf || CONF_DIFF_DHCP4=1

cmp --silent /etc/kea/kea-dhcp6.conf.new /etc/kea/kea-dhcp6.conf || CONF_DIFF_DHCP6=1

if [ $CONF_OK_DHCP4 -eq 1 ] && [ $CONF_DIFF_DHCP4 -eq 1 ]; then
    cp /etc/kea/kea-dhcp4.conf.new /etc/kea/kea-dhcp4.conf
    systemctl restart kea-dhcp4.service isc-kea-dhcp4-server.service || true
fi

if [ $CONF_OK_DHCP6 -eq 1 ] && [ $CONF_DIFF_DHCP6 -eq 1 ]; then
    cp /etc/kea/kea-dhcp6.conf.new /etc/kea/kea-dhcp6.conf
    systemctl restart kea-dhcp6.service isc-kea-dhcp6-server.service || true
fi

# ===================== HOSTS BLOCKLIST =====================
touch /etc/hosts_blocklist
touch /etc/hosts_blocklist.new

wget https://dc.admin.dolphin-it.de/adblock/distribution_download/default -O /etc/hosts_blocklist.new

cmp --silent /etc/hosts_blocklist.new /etc/hosts_blocklist || CONF_DIFF_HOSTS_BLOCKLIST=1

if [ $CONF_DIFF_HOSTS_BLOCKLIST -eq 1 ]; then
    cp /etc/hosts_blocklist.new /etc/hosts_blocklist
    rec_control --timeout=300 reload-zones
fi
