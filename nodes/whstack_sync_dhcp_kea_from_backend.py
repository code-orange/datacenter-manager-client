import json
import os


def work(dcmgr_instance_data):
    paths = ("/etc/kea",)

    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path)

    with open(
        os.path.join("/etc/kea/kea-dhcp4.conf.new"), "w", newline=""
    ) as kea_dhcp4_conf:
        kea_dhcp4_conf.write(json.dumps(dcmgr_instance_data["dhcp4"], indent=4))

    with open(
        os.path.join("/etc/kea/kea-dhcp6.conf.new"), "w", newline=""
    ) as kea_dhcp6_conf:
        kea_dhcp6_conf.write(json.dumps(dcmgr_instance_data["dhcp6"], indent=4))
